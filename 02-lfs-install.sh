
echo "-------------------------------------------------"
echo "-------select your disk to format----------------"
echo "-------------------------------------------------"
lsblk -f
echo "Please enter disk: (example /dev/sda)"
read DISK
echo "--------------------------------------"
echo -e "\nFormatting disk...\n$HR"
echo "--------------------------------------"

#################### Disk Prep ###########################################################

sgdisk -Z ${DISK} # zap all on disk
sgdisk -a 2048 -o ${DISK} # new gpt disk 2048 alignment

#################### Create Partitions ###################################################

sgdisk -n 1:0:+500M ${DISK} # partition 1 (UEFI SYS), default start block, 512MB
sgdisk -n 2:0:+500M ${DISK} # partition 2 (Boot),
sgdisk -n 3:0:0     ${DISK} # partition 3 (LVM), with root and home partition later on

#################### Set Partition Types #################################################

sgdisk -t 1:ef00 ${DISK}
sgdisk -t 2:8300 ${DISK}
sgdisk -t 3:8300 ${DISK}

#################### Label Partitions ####################################################

sgdisk -c 1:"UEFISYS" ${DISK}
sgdisk -c 2:"BOOT"    ${DISK}
sgdisk -c 3:"LVM"     ${DISK}

#################### Make Filesystems ####################################################
echo -e "\nCreating Filesystems...\n$HR"
mkfs.vfat -F32 -n "UEFISYS" ${DISK}1
mkfs.ext4      -n "BOOT"    ${DISK}2

################### LVM Setup  #############################################################

echo "-----------------------------------------"
echo "------- LVM Partitioning ----------------"
echo "-----------------------------------------"

pvcreate ${DISK}3
vgcreate vg0 ${DISK}3 
lvcreate -L 10G vg0 -n root 
lvcreate -l 100%FREE vg0 -n home 
mkfs.ext4 /dev/vg0/root 
mkfs.ext4 /dev/vg0/home 

passwd root
sed -i 's/#Port 22/Port 22/g' /etc/ssh/sshd_config
systemctl restart sshd
export LFS=/mnt/lfs
mkdir -pv $LFS
mount -v -t ext4 /dev/vg0/root $LFS
mkdir -v $LFS/home
mount -v -t ext4 /dev/vg0/home $LFS
mkdir -v $LFS/sources
chmod -v a+wt $LFS/sources
wget --input-file=wget-list --continue --directory-prefix=$LFS/sources

## Creating basic LFS filesystem directory layout
mkdir -pv $LFS/{etc,var} $LFS/usr/{bin,lib,sbin}

for i in bin lib sbin; do
  ln -sv usr/$i $LFS/$i
done

case $(uname -m) in
  x86_64) mkdir -pv $LFS/lib64 ;;
esac

mkdir -pv $LFS/tools

groupadd lfs
useradd -s /bin/bash -g lfs -m -k /dev/null lfs
passwd lfs

chown -v lfs $LFS/{usr{,/*},lib,var,etc,bin,sbin,tools}
case $(uname -m) in
  x86_64) chown -v lfs $LFS/lib64 ;;
esac

chown -v lfs $LFS/sources
cp -r /root/lfs /home/lfs
chown lfs:lfs /home/lfs/lfs
su - lfs

bash 03-*.sh

