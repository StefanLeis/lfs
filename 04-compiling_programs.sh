#!/usr/bin/env bash

#compile binutils Pass 1
cd $LFS/sources
tar -xf binutils-2.37.tar.xz
cd binutils-2.37
mkdir -v build
cd       build
../configure --prefix=$LFS/tools \
             --with-sysroot=$LFS \
             --target=$LFS_TGT   \
             --disable-nls       \
             --disable-werror
make 
make install -j1

pause 5

#compile gcc v- Pass 1

cd $LFS/sources
rm -rf binutils-2.37
tar -xf gcc-11.2.0.tar.xz
cd gcc-11.2.0
tar -xf ../mpfr-4.1.0.tar.xz
mv -v mpfr-4.1.0 mpfr
tar -xf ../gmp-6.2.1.tar.xz
mv -v gmp-6.2.1 gmp
tar -xf ../mpc-1.2.1.tar.gz
mv -v mpc-1.2.1 mpc

case $(uname -m) in
  x86_64)
    sed -e '/m64=/s/lib64/lib/' -i.orig gcc/config/i386/t-linux64
 ;;
esac

mkdir -v build
cd build
../configure                                       \
    --target=$LFS_TGT                              \
    --prefix=$LFS/tools                            \
    --with-glibc-version=2.11                      \
    --with-sysroot=$LFS                            \
    --with-newlib                                  \
    --without-headers                              \
    --enable-initfini-array                        \
    --disable-nls                                  \
    --disable-shared                               \
    --disable-multilib                             \
    --disable-decimal-float                        \
    --disable-threads                              \
    --disable-libatomic                            \
    --disable-libgomp                              \
    --disable-libquadmath                          \
    --disable-libssp                               \
    --disable-libvtv                               \
    --disable-libstdcxx                            \
    --enable-languages=c,c++
make
make install -j1 
cd ..
cat gcc/limitx.h gcc/glimits.h gcc/limity.h > `dirname $($LFS_TGT-gcc -print-libgcc-file-name)`/install-tools/include/limits.h

pause 5

#Compile linux kernel
cd $LFS/sources
rm -rf gcc-11.2.0 mpc gmp mpfr
tar -xf linux-5.13.12.tar.xz
cd linux-5.13.12
make mrproper
make headers
find usr/include -name '.*' -delete
rm usr/include/Makefile
cp -rv usr/include $LFS/usr

pause 5

#compile glibc
cd $LFS/sources
rm -rf linux-5.13.12 
tar -xf glibc-2.34.tar.xz
cd glibc-2.34
case $(uname -m) in
    i?86)   ln -sfv ld-linux.so.2 $LFS/lib/ld-lsb.so.3
    ;;
    x86_64) ln -sfv ../lib/ld-linux-x86-64.so.2 $LFS/lib64
            ln -sfv ../lib/ld-linux-x86-64.so.2 $LFS/lib64/ld-lsb-x86-64.so.3
    ;;
esac
patch -Np1 -i ../glibc-2.34-fhs-1.patch
mkdir -v build
cd       build
echo "rootsbindir=/usr/sbin" > configparms
../configure                             \
      --prefix=/usr                      \
      --host=$LFS_TGT                    \
      --build=$(../scripts/config.guess) \
      --enable-kernel=3.2                \
      --with-headers=$LFS/usr/include    \
      libc_cv_slibdir=/usr/lib
make && make DESTDIR=$LFS install
sed '/RTLDLIST=/s@/usr@@g' -i $LFS/usr/bin/ldd

pause 5

## Test of the system so far for errors
echo 'int main(){}' > dummy.c
$LFS_TGT-gcc dummy.c
readelf -l a.out | grep '/ld-linux'
pause 5

rm -v 'dummy.c a.out'
$LFS/tools/libexec/gcc/$LFS_TGT/11.2.0/install-tools/mkheaders

pause 30

#compile libstdc
cd $LFS/sources
rm -rf glibc-2.34
tar -xf gcc-11.2.0.tar.xz
cd $LFS/sources/gcc-11.2.0/
mkdir -v build
cd build
../libstdc++-v3/configure           \
    --host=$LFS_TGT                 \
    --build=$(../config.guess)      \
    --prefix=/usr                   \
    --disable-multilib              \
    --disable-nls                   \
    --disable-libstdcxx-pch         \
    --with-gxx-include-dir=/tools/$LFS_TGT/include/c++/11.2.0
pause 20
make
make DESTDIR=$LFS install -j1

pause 5

## Cross Compiling Temporary Tools 
 
#compile m4
cd $LFS/sources
rm -rf gcc-11.2.0
tar -xf m4-1.4.19.tar.xz
cd m4-1.4.19
./configure --prefix=/usr   \
            --host=$LFS_TGT \
            --build=$(build-aux/config.guess)
make && make DESTDIR=$LFS install
pause 10

#compile ncurses
cd $LFS/sources
rm -rf m4-1.4.19
tar -xf ncurses-*
cd ncurses-6.2
sed -i s/mawk// configure
mkdir build
pushd build
  ../configure
  make -C include
  make -C progs tic
popd
./configure --prefix=/usr                \
            --host=$LFS_TGT              \
            --build=$(./config.guess)    \
            --mandir=/usr/share/man      \
            --with-manpage-format=normal \
            --with-shared                \
            --without-debug              \
            --without-ada                \
            --without-normal             \
            --enable-widec
make
make DESTDIR=$LFS TIC_PATH=$(pwd)/build/progs/tic install
echo "INPUT(-lncursesw)" > $LFS/usr/lib/libncurses.so
pause 10

#compile bash
cd $LFS/sources
rm -rf ncurses-6.2
tar -xf bash-*
cd bash-5.1.8
./configure --prefix=/usr                   \
            --build=$(support/config.guess) \
            --host=$LFS_TGT                 \
            --without-bash-malloc
ln -sv bash $LFS/bin/sh
pause 10

#compile coreutils
cd $LFS/sources
rm -rf bash-5.1.8
tar -xf coreutils-8.32.tar.xz
cd coreutils-8.32
./configure --prefix=/usr                     \
            --host=$LFS_TGT                   \
            --build=$(build-aux/config.guess) \
            --enable-install-program=hostname \
            --enable-no-install-program=kill,uptime
make && make DESTDIR=$LFS install
mv -v $LFS/usr/bin/chroot $LFS/usr/sbin
mkdir -pv $LFS/usr/share/man/man8
mv -v $LFS/usr/share/man/man1/chroot.1 $LFS/usr/share/man/man8/chroot.8
sed -i 's/"1"/"8"/' $LFS/usr/share/man/man8/chroot.8
pause 10

#compile diffutils
cd $LFS/sources
rm -rf coreutils-8.32
tar -xf diffutils-*
cd diffutils-3.8
./configure --prefix=/usr --host=$LFS_TGT
make && make DESTDIR=$LFS install
pause 10

#compile File
cd $LFS/sources
rm -rf diffutils-3.8
tar -xf file-*
cd file-5.40
mkdir build
pushd build
../configure --disable-bzlib --disable-libseccomp --disable-xzlib --disable-zlib
 make
popd
./configure --prefix=/usr --host=$LFS_TGT --build=$(./config.guess)
make FILE_COMPILE=$(pwd)/build/src/file && make DESTDIR=$LFS install
pause 10

#compile Findutils
cd $LFS/sources
rm -rf file-5.40
tar -xf findutils-*
cd findutils-4.8.0
./configure --prefix=/usr                   \
            --localstatedir=/var/lib/locate \
            --host=$LFS_TGT                 \
            --build=$(build-aux/config.guess)
make && make DESTDIR=$LFS install
pause 10

#compile Gawk
cd $LFS/sources
rm -rf findutils-4.8.0
tar -xf gawk-*
cd gawk-5.1.0
sed -i 's/extras//' Makefile.in
./configure --prefix=/usr   \
            --host=$LFS_TGT \
            --build=$(./config.guess)
make && make DESTDIR=$LFS install
pause 5

#compile Grep
cd $LFS/sources
rm -rf gawk-5.1.0
tar -xf grep-*
cd grep-3.7
./configure --prefix=/usr --host=$LFS_TGT
make && make DESTDIR=$LFS install
pause 5

#compile Gzip
cd $LFS/sources
rm -rf grep-5.1.0
tar -xvf gzip-*
cd gzip-1.10
./configure --prefix=/usr --host=$LFS_TGT
make && make DESTDIR=$LFS install
pause 5

#compile Make
cd $LFS/sources
rm -rf gzip-1.10
tar -xf make-*
cd make-4.3
./configure --prefix=/usr   \
            --without-guile \
            --host=$LFS_TGT \
            --build=$(build-aux/config.guess)
make && make DESTDIR=$LFS install
paus 5

#compile Patch
cd $LFS/sources
rm -rf make-4.3
tar -xf patch-*
cd patch-2.7.6
./configure --prefix=/usr   \
            --host=$LFS_TGT \
            --build=$(build-aux/config.guess)
make && make DESTDIR=$LFS install
pause 5

#compile Sed
cd $LFS/sources
rm -rf patch-2.7.6
tar -xf sed-*
cd sed-4.8 
./configure --prefix=/usr --host=$LFS_TGT
make && make DESTDIR=$LFS install
pause 5

#compile Tar
cd $LFS/sources
rm -rf sed-4.8
tar -xf tar-*
cd tar-1.34
./configure --prefix=/usr --host=$LFS_TGT --build=$(build-aux/config.guess)
make && make DESTDIR=$LFS install
pause 5

#compile Xz
cd $LFS/sources
rm -rf tar-1.34
tar -xf xz-*
cd xz-5.2.5
./configure --prefix=/usr                     \
            --host=$LFS_TGT                   \
            --build=$(build-aux/config.guess) \
            --disable-static                  \
            --docdir=/usr/share/doc/xz-5.2.5
make && make DESTDIR=$LFS install
pause 5

#compile Binutils Pass 2
cd $LFS/sources
rm -rf binutils-2.37
tar -xf binutils-2.37.tar.xz
cd binutils-2.37
mkdir -v build
cd build
../configure                   \
    --prefix=/usr              \
    --build=$(../config.guess) \
    --host=$LFS_TGT            \
    --disable-nls              \
    --enable-shared            \
    --disable-werror           \
    --enable-64-bit-bfd
make && make DESTDIR=$LFS install
install -vm755 libctf/.libs/libctf.so.0.0.0 $LFS/usr/lib
pause 10

#compile GCC - Pass 2

cd $LFS/sources
rm -rf xz-5.2.5
rm -rf binutils-2.37
rm -rf gcc-11.2.0 mpfr gmp mpc
tar -xf gcc-11.2.0.tar.xz
cd gcc-11.2.0
tar -xf ../mpfr-4.1.0.tar.xz
mv -v mpfr-4.1.0 mpfr
tar -xf ../gmp-6.2.1.tar.xz
mv -v gmp-6.2.1 gmp
tar -xf ../mpc-1.2.1.tar.gz
mv -v mpc-1.2.1 mpc
case $(uname -m) in
  x86_64)
    sed -e '/m64=/s/lib64/lib/' -i.orig gcc/config/i386/t-linux64
  ;;
esac
mkdir -v build
cd       build
mkdir -pv $LFS_TGT/libgcc
ln -s ../../../libgcc/gthr-posix.h $LFS_TGT/libgcc/gthr-default.h
../configure                                       \
    --build=$(../config.guess)                     \
    --host=$LFS_TGT                                \
    --prefix=/usr                                  \
    CC_FOR_TARGET=$LFS_TGT-gcc                     \
    --with-build-sysroot=$LFS                      \
    --enable-initfini-array                        \
    --disable-nls                                  \
    --disable-multilib                             \
    --disable-decimal-float                        \
    --disable-libatomic                            \
    --disable-libgomp                              \
    --disable-libquadmath                          \
    --disable-libssp                               \
    --disable-libvtv                               \
    --disable-libstdcxx                            \
    --enable-languages=c,c++
make && make DESTDIR=$LFS install
ln -sv gcc $LFS/usr/bin/cc
