#!/usr/bin/env bash

#LFS preperations

PKGS=(
'acl'
'attr'
'automake'
'bash'
'base-devel'
'bc'
'binutils'
'bison'
'dejaGNU'
'diffutils'
'e2fsprogs'
'expat'
'file'
'gcc'
'gdbm'
'glibc'
'grep'
'grub'
'iana-etc'
'iproute2'
'kmod'
'less'
'libcap'
'libelf'
'libffi'
'linux'
'm4'
'man-pages'
'meson'
'openssl'
'perl'
'python'
'python3'
'shadow'
'systemd'
'texinfo'
'util-linux'
'vim'
'zstd'
)

for PKG in "${PKGS[@]}"; do
    echo "INSTALLING: ${PKG}"
    sudo pacman -S "$PKGS" --noconfirm --needed
done
